<?php

namespace App\Controller;

use App\Repository\VideoRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(
        VideoRepository $videoRepository,
        Request $request
    ): Response {
        $videos = $videoRepository->findVideosByCategory($request->query->getInt('page', 1));

        return $this->render('pages/home/index.html.twig', [
            'videos' => $videos,
            'video_description_length' => VideoRepository::VIDEO_DESCRIPTION_LENGTH,
        ]);
    }
}
