<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @extends ServiceEntityRepository<Video>
 *
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public const VIDEO_DESCRIPTION_LENGTH = 90;
    public const MAX_VIDEOS_PER_PAGE = 6;

    public function __construct(
        ManagerRegistry $registry,
        private PaginatorInterface $paginatorInterface,
        )
    {
        parent::__construct($registry, Video::class);
    }

    public function save(Video $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Video $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Find videos by category
     *
     * @param integer $page
     * @param object|null $category
     * @return PaginationInterface
     */
    public function findVideosByCategory(int $page, object $category = null): PaginationInterface
    {
        if ($category === null) {
            $data =  $this->createQueryBuilder('v')
                ->orderBy('v.createdAt', 'DESC')
                ->getQuery()
                ->getResult()
            ;
        } else {
            $data = $this->createQueryBuilder('v')
                ->leftJoin('v.category', 'c')
                ->where('c.slug = :slug')
                ->setParameter('slug', $category->getSlug())
                ->orderBy('v.createdAt', 'DESC')
                ->getQuery()
                ->getResult()
            ;
        }

        $videos = $this->paginatorInterface->paginate($data, $page, self::MAX_VIDEOS_PER_PAGE);

        return $videos;
    }
}
