# tools



## Grumpphp

- Install GrumPHP with dependencies:

```
composer require --dev phpro/grumphp
```

- copy/paste the file in "grumphp.yml"


## Makefile

- create a file with the name "Makefile"
- copy/paste the file


## Gitlab-ci

- create a file with the name ".gitlab-ci.yml"
- copy/paste the file


## UserListener

- create in the "src" a new folder "EntityListener"
- create a file with the name "UserListener.php"
- add in "User" entity :
```
#[ORM\EntityListeners(['App\EntityListener\UserListener'])]
```
- modify in fixtures SetPassword('password') by SetPlainPassword('password) to save in database
```
$user->setEmail('admin@admin.fr')
            ->setPlainPassword('password')
```


## LoginController

- create LoginController with the data in LoginController
- modify "security.yaml"
```
form_login:
    # "app_login" is the name of the route created previously
    login_path: app_login
    check_path: app_login
    enable_csrf: true
logout:
    path: app_logout
```


## Pagination
- install the library
```
composer require knplabs/knp-paginator-bundle
```
- create in the folder "config/packages" the file "knp_paginator.yaml"
- copy/paste the same file in "pagination"
- create in the folder "Service", the file "PaginationService"
- render the view with "pagination/PaginationService.php"
- inject the service in the controller
- render the view with "pagination/home.html.twig"


